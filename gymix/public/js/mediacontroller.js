var MediaController = function(opts){
    this.audioPlayer = opts.audioPlayer;
    this.videoPlayer = opts.videoPlayer;
    this.audioSource = opts.audioSource;
    this.playlist = { "music" : [], "video" : { "adverts": []}};

    this.tracks = this.audioPlayer.textTracks;
    this.cues = opts.cues;
    this.currentPlaylistPosition = 0;

    if(opts.playlist){

       if( opts.playlist.hasOwnProperty("music")){
        var len = opts.playlist.music.length;
        console.log("playlist length: ", len);
        for(var i = 0; i < len; i++){
            
            var playlistItem = opts.playlist.music[i];
            
            if(playlistItem && playlistItem.hasOwnProperty("file") &&
               playlistItem.hasOwnProperty("vtt")){
                console.log("playlistItem: ", playlistItem);
                var file = playlistItem.file;
                var vtt = playlistItem.vtt;
                this.playlist.music.push({ "file": file,
                                           "vtt": vtt});
            }
        }
       }

       if(opts.playlist.hasOwnProperty("video") && opts.playlist.video.hasOwnProperty("adverts")){
          for(var i = 0; i < opts.playlist.video.adverts.length;i++){
            this.playlist.video.adverts.push(opts.playlist.video.adverts[i]);
            }
       }
    }
 
};

MediaController.prototype.init = function(){
   var self = this;
   if(this.playlist.music.length <= 0){ 

   return;}

   var firstEntry = this.playlist.music[0];
   console.log(firstEntry);
   $('#videoPlayerContainer').hide();
   
   this.videoPlayer.addEventListener("ended", function(){
        $('#videoPlayerContainer').hide();
        $('#audioPlayerContainer').show();
        self.currentTrackNumber++;
        self.audioPlayer.play();
    })
   
   self.cues.src = "/media/music/" + firstEntry.vtt;
   self.audioPlayer.src = "/media/music/" + firstEntry.file;
   console.log("video ads ", this.playlist.video.adverts);
   self.videoPlayer.src = "/media/adverts/" + this.playlist.video.adverts[0];

   this.tracks[0].addEventListener("cuechange", function(event){
         
        console.log("track changed");
        console.log(event);
        
        if(event.currentTarget.activeCues && event.currentTarget.activeCues.length > 0){
        $('#songname').html(event.currentTarget.activeCues[0].text);
        }
        //self.cues.track.removeCue(self.cues.track.cues[0]);
        //if(event.currentTarget.activeCues[0].id != 1
        //   && event.currentTarget.activeCues[0].id != 2){
         if(event.currentTarget.activeCues[0].id != 1){
            self.audioPlayer.pause();
            $('#audioPlayerContainer').hide();
            $('#videoPlayerContainer').show();
            self.videoPlayer.play();
        }

        

    });

    this.audioPlayer.addEventListener("ended",function(){
        console.log("audio ended");
        self.currentPlaylistPosition++;
        console.log(self.cues.track.cues);
        var textTracks = self.audioPlayer.textTracks;
        var textTrack = textTracks[0];
        var cues = textTrack.cues;

        for(var i = cues.length - 1; i>=0; i--){
            console.log("removing cue ", cues[i]);
            textTrack.removeCue(cues[i]);
        }

        var nextEntry = {};
        if(self.currentPlaylistPosition === self.playlist.music.length){
            self.currentPlaylistPosition = 0;
        }
    
        nextEntry = self.playlist.music[self.currentPlaylistPosition];
        self.cues.src = "/media/music/" + nextEntry.vtt;
        self.audioPlayer.src = "/media/music/" + nextEntry.file;

    
    });

    self.audioPlayer.addEventListener("canplay",function(){
        console.log("Can Play");
        self.audioPlayer.play();
    });
};
