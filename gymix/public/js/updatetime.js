
function updateTime() {
	var currentTime = new Date();
	var hours = currentTime.getHours();
	var minutes = currentTime.getMinutes();
	var seconds = currentTime.getSeconds();
			
    if (minutes < 10){
	    minutes = "0" + minutes;
	}
	
    if (seconds < 10){
		seconds = "0" + seconds;
	}
	
    var v = hours + ":" + minutes;
	$("date").innerHTML = currentTime.toDateString();
	setTimeout(updateTime,60000);
	$('#time').html(v);
}

