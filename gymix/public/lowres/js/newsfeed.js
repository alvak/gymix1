$("#newsfeed > div:gt(0)").hide();

function startNewsFeed(){	
    setInterval(function() { 
	    $('#newsfeed > div:first')
	    .fadeIn()
	    .next()
	    .fadeOut()
	    .end()
	    .appendTo('#newsfeed');
	},  10000);
};			
									
