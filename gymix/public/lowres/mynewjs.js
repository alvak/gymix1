	<script>
     $(function(){
		

        updateTime();
        refreshContent();

        setInterval(function () {
				var currentTime = new Date();
				var minutes = currentTime.getMinutes();
				var seconds = currentTime.getSeconds();

				if (((minutes == 0) || (minutes == 15) || (minutes == 30) || (minutes == 45)) && (seconds == 0)){
							refreshContent();
				 }
		}, 1000);

        startNewsFeed();

    var mediaController = null;

    var socket = io("localhost:8080", { transports : ["websocket","xhr-polling"]});

    socket.on("connect", function(){
        console.log("websocket connected");
        socket.emit("playlist:current",{});
       
    });

    socket.on("disconnect", function(){
        console.log("websocket disconnected");
    });

    socket.on("playlist:current", function(playlist){
        console.log(playlist);
        if( ! mediaController ){
            mediaController = new MediaController({
                "videoPlayer" : document.getElementById("videoPlayer"),
                "audioPlayer" : document.getElementById("audioPlayer"),
                "audioSource" : document.getElementById("audioSource"),
                "cues"       : document.getElementById("cues"),
                "playlist" : playlist}
            );
            mediaController.init(playlist);
        }
    });

   socket.on("playlist:refresh", function(){
       console.log("received playlist:refresh should update everything now and restart");
       location.reload(true); 

   });
});


        <script src="js/updatetime.js"></script>
        <script src="js/timetable.js"></script>
        <script src="js/newsfeed.js"></script>
        <script src="/socket.io/socket.io.js"></script>
        <script src="js/mediacontroller.js"></script>

<link rel="stylesheet" media="screen and (min-width: 1600px)" href="webapp2.css" />
