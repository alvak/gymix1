/**
 * http://usejsdoc.org/
 */
var fs = require("fs");
var express = require("express");
var app = express();
var CueConvert = require("../lib/CueConvert");


app.use(express.static(__dirname + "/public"));
function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

fs.readdir("media/music",
        function(error, cuefiles)
        {
  		  console.log("SIZE:"+cuefiles.length);
  		  for (i=0;i<cuefiles.length;i++)
  		  {
  		  
  			  if( endsWith(cuefiles[i], ".cue")){
  				  		var file = cuefiles[i];
  				  		console.log(cuefiles[i]);
  				  		var parsedCueSheet = CueConvert.convertCueSheet("media/music/"+file);
  		                file= file.replace(".cue",".vtt");
  		                fs.writeFileSync( "media/music/"+file,parsedCueSheet);
  			}
  		  }
        }
);
