"use strict";

var async = require("async");

var exports = module.exports = {};

var fs = require("fs");
var endsWith= function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

exports.endsWith= function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

exports.getSongTitle= function getSongTitle(str) {
	var idx =str.indexOf( "-");
	console.log(str+" "+idx);
	if (idx==-1 || idx>str.length-2) return str;
    return str.substring(idx+2, str.length);
}

exports.getCurrentMediaList = function( callback ){

   async.parallel([
     function(callback){
                       fs.readdir("public/media/music",
                                        function(error, audioFiles){
                        var files = [];
                        var result = { "music" : [] };
                        if(error){
                            return callback(error, {});
                        }
                        else{
                           var fileObjs = {};
                           audioFiles.forEach( function(file){
                                var tmpAudioFile = "";   

                                if(endsWith(file, ".mp3")){
                                    tmpAudioFile = file.replace(".mp3","");
                               
                                    if(fileObjs.hasOwnProperty(tmpAudioFile)){
                                        fileObjs[tmpAudioFile].file = file;
                                    }
                                    else{
                                        fileObjs[tmpAudioFile] = { "file": file}
                                    } 
                                     // files.push( {"file" :  "media/music/" + file,
                                     //            "vtt"  :   "media/music/" + tmpAudioFile + ".vtt"});
                                }

                                if(endsWith(file, ".vtt")){
                                    tmpAudioFile = file.replace(".vtt","");
                                  
                                    if(fileObjs.hasOwnProperty(tmpAudioFile)){
                                        fileObjs[tmpAudioFile].vtt = file;
                                    }
                                    else{
                                        fileObjs[tmpAudioFile] = { "vtt": file};
                                    }
                                  
                                }
                                
                           });

                           for(var prop in fileObjs){
                              if(fileObjs.hasOwnProperty(prop)){
                                files.push(fileObjs[prop]);
                              }
                           }
                           
                           result.music = files;
                           return callback(null, result);
                        }
                    });
                },
                function(callback){
                       fs.readdir("public/media/adverts",
                                        function(error, advertFiles){
                        var result = { "adverts" : []};
                        var files = [];
                        if(error){
                            return callback(error, {});
                        }
                        else{
                            advertFiles.forEach(function(file){
                            	console.log("file"+file);
                                
//                            	if(file.endsWith(".mp4")){
  //                                  files.push(file);
    //                            }
                            });
                        result.adverts = files;
                            return callback(null,result);
                        }
                    });

                }

            ], function(error, result){     
                var playlist = { "music" : [], "video" : { "adverts" :[]}};

                if(error){
                    console.log(error);
                    callback(error, {});
                }
                else{
                    console.log(result);
                    result.forEach( function(content){
                        if(content.hasOwnProperty("music")){
                            playlist.music = content.music;
                        }
                        else if(content.hasOwnProperty("adverts")){
                            playlist.video.adverts = content.adverts;
                        }
                    });

                    return callback(null, playlist);                   
                }
            }); 
};
