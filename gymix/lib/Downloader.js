var async = require("async");
var EventEmitter = require("events");
var AWS = require("aws-sdk");
var s3 = new AWS.S3();
var downloader = new EventEmitter();
var fs = require("fs");
var basedir = "/home/odroid/gymix/downloads/";
exports.downloader = downloader;

var download = function( file, callback){
    console.log("download file ", file);
    var s3 = new AWS.S3();
    var stream = fs.createWriteStream( basedir + file, { flags: 'w', encoding: null, mode: 0666 });
                    var isError = false;
                    s3.getObject( {
                            Bucket: "gymix",
                            Key: "media/"+file
                    })
                    .on('httpData', function(chunk){
                        stream.write(chunk);
                    })
                    .on("success", function(response){
                        console.log("success");
                        
                    })
                    .on("error", function(error){
                        console.log(error);
                        isError = true;
                    })
                    .on('complete',function(){
                        stream.end();
                        if(isError){
                            fs.unlinkSync(basedir + file);
                            callback("error");
                        }
                        else{
                        console.log("file downloaded", file); 
                        callback(); 
                        }
                    })
                    .send();
    
};


module.exports.downloadFiles = function(filesToDownload){
    console.log("files to download");

    console.log(filesToDownload);

    var downloadFunctions = [];

   filesToDownload.download.forEach(function(file){
         downloadFunctions.push( function(callback){ download(file,callback);});

   });

  async.series(downloadFunctions, function(err, done){  
      if(err){     
          downloader.emit("downloads:error",{});
      }
      else{
          downloader.emit("downloads:complete", filesToDownload);
     }
  });
};

module.exports.event = downloader;
