var AWS = require('aws-sdk');
var fs = require('fs');
var s3 = new AWS.S3();

var  util = require("./lib/Util");

var exports = modules.exports = {};

/*s3.listObjects({
   Bucket: "gymix",
   Delimiter: "/",
   Prefix: "adverts/"
},function(err, data){
    if(err) console.log(err);
    console.log(data);
});*/

//var stream = fs.createWriteStream(__dirname + "/syncdir/promo.mp4", { flags: 'w', encoding: null, mode: 0666 });
/*var downloader = s3.getObject( {
   Bucket: "gymix",
   Key: "latest.json"
})
.on('httpData', function(chunk){
    stream.write(chunk);
})
.on("success", function(response){
    console.log(response);
})
.on("error", function(error){
    console.log(error);
})
.on('complete',function(){
   stream.end();
   console.log("file downloaded");  
})
.send();*/

var latestListRetrieved = function(latest){
    util.getCurrentMediaList(function(err, current){
        var currentMP3List = [];
        var latestMP3List = [];

        latest.music.forEach(function(latestMusicFile){
            latestMP3List.push(latestMusicFile.file);
        });
        latestMP3List.sort();
        console.log("Latest MP3s " , latestMP3List);
        if(err){
            console.log(err);
        }
        else {
           
            current.music.forEach(function(playlistItem){
                playlistItem = playlistItem.file.replace("media/music/", "");
                currentMP3List.push(playlistItem);
            });
            currentMP3List.sort();
            console.log("Current MP3 List", currentMP3List);
            
        }

        if( latestMP3List.length !== currentMP3List.length ){
            if(latestMP3List.length > 0 ){
                console.log("List Lengths Not the same, triggering download");
            }
        } else if( latestMP3List.length === currentMP3List.length){
            var performUpdate = false;
            console.log("lists are same length");
            for(var idx = 0; idx < latestMP3List.length; idx++){
                if(latestMP3List[idx] !== currentMP3List[idx]){
                    console.log("lists arent same, triggering download");
                    performUpdate = true;
                    break;
                }
            }
            console.log("Perform Update = ", performUpdate);
        }
             
    });
};

exports.checkForUpdates = function(){
    var downloader = s3.getObject({
        Bucket: "gymix",
        Key: "music/latest.json"
    }, function( err, data){
        if( err ){ 
            console.log( err)
        }
        else{
            var latest = data.Body.toString();

            var latestObj = JSON.parse(latest);   
            console.log(latestObj);
            latestListRetrieved(latestObj);
        }
    });
};
