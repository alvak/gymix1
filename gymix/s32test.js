var s3 = require("s3");
var aws = require("aws-sdk");


var c = new aws.S3({signatureVersion: 'v4', region: 'eu-west-1'});
var client = s3.createClient( { s3Client : c}) 

var params = {
    localDir: "/home/garysmi/git/gymix/syncdir", 
    s3Params: {
       Bucket: "gymix",
       Prefix: "adverts/",
       Delimiter: "/"
    }

};

var downloader = client.downloadDir({
   s3Params: {
     Bucket: "gymix",
     Prefix: "adverts/",
     Delimiter: "/"
  },
  localDir : '/home/garysmi/git/gymix/syncdir' 
});


downloader.on('error',function(error){
   console.log(error);
})
